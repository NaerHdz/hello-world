from django.urls import path

from .views import HomePageView, AcercadePageView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('acercade/', AcercadePageView.as_view(), name='acercade')
]