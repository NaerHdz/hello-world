#from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView

#def homePageView(request):
#  return HttpResponse('Hello, World!')


class HomePageView(TemplateView):
  template_name = 'home.html'

class AcercadePageView(TemplateView):
  template_name = 'acercade.html'